<?php

/**
 * @file
 * Workbench Extra admin file.
 */

/**
 * @file
 * Workbench Access admin file.
 */

/**
 * Settings form for Workbench Extra configuration.
 */
function workbench_extra_settings_form($form, &$form_state) {
  $form['workbench_extra_permission_by_state'] = array(
    '#type' => 'checkbox',
    '#title' => t('Edit-permission by state'),
    '#default_value' => variable_get('workbench_extra_permission_by_state', 0),
    '#description' => t('Enable edit-permission by state. Once enabled, see normal permissions admin page to configure.<br/> Available if workbench_moderation is enable.'),
    '#access' => module_exists('workbench_moderation'),
  );
  $form = system_settings_form($form);
  return $form;
}
